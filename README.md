# Go-Plan

Go-Plan merupakan aplikasi yang memudahkan seseorang untuk mencari sebanyak mungkin point of interest yang dapat dikunjungi dalam suatu batasan waktu yang diinginkan.

- User dibatasi hanya mengunjungi tempat-tempat atau point of interest yang sudah didefinisikan dalam database Go-Plan
- User akan diberikan rentang durasi yang diperbolehkan (misalnya dalam 6 jam)
- Ruang lingkup yang dijadikan range peta adalah Jakarta
- User hanya dapat melakukan pencarian rute, tidak dapat melakukan navigasi terhadap rute yang didapatkan